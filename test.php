map $http_user_agent $bad_bot {
  default 0;
   ~(?i)(Ahrefsbot|Baiduspider|YandexBot|Abonti|ShopWiki|FatBot|UnisterBot|Baidu|BLEXBot|Twengabot|httrack|WinHTTrack|htmlparser|urllib|Zeus|scan|email|WebCollector|WebCopier|WebCopy|webcraw|LWP::simple|Havij) 1;
}

map $uri   $authentication {
   default "Access Restricted";
    ~/Img212/*    off;
    }

server {
  listen 80;
  server_name newdesign.test.itshot.com;
  return 301 https://$host$request_uri;
}

server {
    listen 8080;
    {{server_name}};
    client_max_body_size 50m;
    {{root}};

    {{nginx_access_log}};
    {{nginx_error_log}};

    if ($bad_bot = 1) {
      return 403;
    }

    location /healthcheck {
      return 200;
    }

    rewrite ^/minify/([0-9]+)(/.*.(js|css))$ /lib/minify/m.php?f=$2&d=$1 last;
    rewrite ^/skin/m/([0-9]+)(/.*.(js|css))$ /lib/minify/m.php?f=$2&d=$1 last;

    location /lib/minify/ {
     allow all;
    }

    location ~ (/(app/|includes/|pkginfo/|var/|report/config.xml)|/\.+) {
      deny all;
    }

    try_files $uri $uri/ /index.php?$args;
    index index.php index.html index.htm;

    location ~ \.php$ {
       include fastcgi_params;
       fastcgi_intercept_errors on;
       fastcgi_index index.php;
       fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
       try_files $uri =404;
       fastcgi_pass 127.0.0.1:{{php_fastcgi_port}};
       fastcgi_read_timeout 3600;
       fastcgi_send_timeout 3600;
       fastcgi_param HTTPS "on";
       fastcgi_param SERVER_PORT 80;
       fastcgi_param PHP_VALUE "
         error_log={{php_error_log}};
         memory_limit=512M;
         max_execution_time=90;";
       #fastcgi_param MAGE_RUN_CODE "store_code";
       #fastcgi_param MAGE_RUN_TYPE "website";
       #limit_req zone=limit burst=5 nodelay;
    }

    gzip                on;
    gzip_disable        "msie6";
    gzip_vary           on;
    gzip_proxied        any;
    gzip_comp_level     5;
    gzip_buffers        16 8k;
    gzip_http_version   1.1;
    gzip_types          text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript image/png image/gif image/jpeg application/javascript image/svg+xml;

    location ~* ^.+\.(css|js|jpg|jpeg|gif|png|ico|gz|svg|svgz|ttf|otf|woff|eot|mp4|ogg|ogv|webm|zip|swf)$ {
      add_header Access-Control-Allow-Origin "*";
      expires max;
      access_log off;
    }

    if (-f $request_filename) {
      break;
    }

    location ~/Img212/ {
       proxy_set_header X-Real-IP $remote_addr;
       proxy_set_header X-Forwarded-For $remote_addr;
       proxy_set_header X-Forwarded-Host $http_host;
       proxy_set_header Host $host;
       proxy_set_header HTTPS $fastcgi_https;
       proxy_pass http://127.0.0.1:88;
       proxy_hide_header Via;
       proxy_hide_header Vary;
       proxy_hide_header X-Server;
       #proxy_hide_header X-Backend-Server;
       proxy_max_temp_file_size 0;
       proxy_connect_timeout      7200;
       proxy_send_timeout         7200;
       proxy_read_timeout         7200;
       proxy_buffer_size          128k;
       proxy_buffers              4 256k;
       proxy_busy_buffers_size    256k;
       proxy_temp_file_write_size 256k;
    }
}

server {
  {{server_name}};
  {{ssl_listener}};
  {{ssl_certificate}};
  {{ssl_certificate_key}};
  ssl_session_cache  builtin:1000  shared:SSL:10m;
  ssl_session_timeout  10m;
  ssl_protocols TLSv1.1 TLSv1.2;
  ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128:AES256:AES:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK';
  ssl_prefer_server_ciphers on;
  ssl_stapling on;
  ssl_stapling_verify on;
  client_max_body_size 50m;
  {{root}};

  {{nginx_access_log}};
  {{nginx_error_log}};

  if ($bad_bot = 1) {
    return 403;
  }

  location /healthcheck {
    return 200;
  }

  rewrite ^/minify/([0-9]+)(/.*.(js|css))$ /lib/minify/m.php?f=$2&d=$1 last;
  rewrite ^/skin/m/([0-9]+)(/.*.(js|css))$ /lib/minify/m.php?f=$2&d=$1 last;

  gzip                on;
  gzip_disable        "msie6";
  gzip_vary           on;
  gzip_proxied        any;
  gzip_comp_level     9;
  gzip_buffers        16 8k;
  gzip_http_version   1.1;
  gzip_types          text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript image/png image/gif image/jpeg application/javascript image/svg+xml;

  location ~* ^.+\.(css|js|jpg|jpeg|gif|png|ico|gz|svg|svgz|ttf|otf|woff|eot|mp4|ogg|ogv|webm|zip|swf)$ {
    add_header Access-Control-Allow-Origin "*";
    expires max;
    access_log off;
  }

  location ~ (/(app/|includes/|pkginfo/|var/|report/config.xml)|/\.+) {
    deny all;
  }

  if (-f $request_filename) {
    break;
  }
  location ~ /api/ {
    proxy_pass http://127.0.0.1:8080;
    proxy_set_header Host $http_host;
    proxy_set_header X-Forwarded-Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    #proxy_hide_header X-Varnish;
    proxy_hide_header Via;
    proxy_hide_header Vary;
    proxy_hide_header X-Server;
    proxy_hide_header X-Backend-Server;
    proxy_hide_header X-Frame-Options;
    proxy_redirect off;
    proxy_max_temp_file_size 0;
    proxy_connect_timeout      720;
    proxy_send_timeout         720;
    proxy_read_timeout         720;
    proxy_buffer_size          128k;
    proxy_buffers              4 256k;
    proxy_busy_buffers_size    256k;
    proxy_temp_file_write_size 256k;
  }
  location ~/Img212/ {
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $remote_addr;
    proxy_set_header X-Forwarded-Host $http_host;
    proxy_set_header Host $host;
    proxy_set_header HTTPS "on";
    proxy_pass http://127.0.0.1:88;
    proxy_hide_header Via;
    proxy_hide_header Vary;
    proxy_hide_header X-Server;
    #proxy_hide_header X-Backend-Server;
    proxy_max_temp_file_size 0;
    proxy_connect_timeout      7200;
    proxy_send_timeout         7200;
    proxy_read_timeout         7200;
    proxy_buffer_size          128k;
    proxy_buffers              4 256k;
    proxy_busy_buffers_size    256k;
    proxy_temp_file_write_size 256k;
  }

  location / {
    auth_basic  "Restricted";
    auth_basic_user_file /home/cloudpanel/.data/basic_auth/test_environment;
    proxy_pass http://127.0.0.1:6081;
    proxy_set_header Host $http_host;
    proxy_set_header X-Forwarded-Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    #proxy_hide_header X-Varnish;
    proxy_hide_header Via;
    proxy_hide_header Vary;
    proxy_hide_header X-Server;
    proxy_hide_header X-Backend-Server;
    proxy_hide_header X-Frame-Options;
    proxy_redirect off;
    proxy_max_temp_file_size 0;
    proxy_connect_timeout      720;
    proxy_send_timeout         720;
    proxy_read_timeout         720;
    proxy_buffer_size          128k;
    proxy_buffers              4 256k;
    proxy_busy_buffers_size    256k;
    proxy_temp_file_write_size 256k;
  }
}

server {
  listen 88;
  {{server_name}};
  client_max_body_size 50m;
  {{root}};

  {{nginx_access_log}};
  {{nginx_error_log}};

  rewrite ^/minify/([0-9]+)(/.*.(js|css))$ /lib/minify/m.php?f=$2&d=$1 last;
  rewrite ^/skin/m/([0-9]+)(/.*.(js|css))$ /lib/minify/m.php?f=$2&d=$1 last;

  location /lib/minify/ {
    allow all;
  }

  gzip                on;
  gzip_disable        "msie6";
  gzip_vary           on;
  gzip_proxied        any;
  gzip_comp_level     8;
  gzip_buffers        16 8k;
  gzip_http_version   1.1;
  gzip_types text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript image/png image/gif image/jpeg application/javascript image/svg+xml;

  location ~* ^.+\.(css|js|jpg|jpeg|gif|png|ico|gz|svg|svgz|ttf|otf|woff|eot|mp4|ogg|ogv|webm|zip|swf)$ {
    expires max;
    access_log off;
  }

  location ~ (/(app/|includes/|pkginfo/|var/|report/config.xml)|/\.+) {
    deny all;
  }

  try_files $uri $uri/ /index.php?$args;
  index index.php index.html index.htm;

  location ~ \.php$ {
    include fastcgi_params;
    fastcgi_intercept_errors on;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    try_files $uri =404;
    fastcgi_pass 127.0.0.1:{{php_fastcgi_port}};
    fastcgi_read_timeout 36000;
    fastcgi_send_timeout 36000;
    fastcgi_param HTTPS "on";
    #fastcgi_param HTTPS $fastcgi_https;
    fastcgi_param SERVER_PORT 80;
    fastcgi_param PHP_VALUE "
      error_log={{php_error_log}};
      memory_limit=2048M;
      max_execution_time=7200;";
    #fastcgi_param MAGE_RUN_CODE "store_code";
    #fastcgi_param MAGE_RUN_TYPE "website";
   }
}